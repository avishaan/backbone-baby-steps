
$('#releaseDate').datepicker();

	var Book = Backbone.Model.extend({
		defaults: {
			coverImage: 'img/cover.png',
			title: 'Default Title',
			author: 'John Doe',
			releaseDate: '2000',
			keywords: 'book'
		}, 
        idAttribute: '_id'
	});

	var Library = Backbone.Collection.extend({
        model: Book,
        url: '/api/books'
        });

	var BookView = Backbone.View.extend({
		className: 'bookContainer',
		tagName: 'div',
		template: $('#bookTemplate').html(),
		events: {
			"click .delete" : 'deleteBook'
		},
		render: function() {
			var tmpl = _.template(this.template);
			this.$el.html(tmpl(this.model.toJSON()));
			return this;
		},
		deleteBook: function() {
			this.model.destroy();
			this.$el.fadeOut(305);
		}
	});

	var LibraryView = Backbone.View.extend({
		el: $('#books'),
		events: {
			"click #add": 'addBook'
		},
		initialize: function() {
			this.collection.fetch();
            this.render();

			this.collection.on("add", this.renderBook, this);
            this.collection.on("remove", this.removeBook, this);
            this.collection.on("reset", this.render, this); //this event is called when we fetch
		},
		render: function() {
			this.collection.each(this.renderBook,this);
		},
		renderBook: function(book) {
			var bookView = new BookView({model:book});
			this.$el.append(bookView.render().el);
		},
		addBook: function(e) {
			e.preventDefault();
			var formData = {};

			$('#addBook').find('input').each(function(i, el){
				if ($(el).val() !== "" ){
                    if (el.id === "releaseDate") {
                        formData[el.id] = $('#releaseDate').datepicker('getDate').getTime();
                    } else if (el.id === "keywords") {
                        var keywordArray = $(el).val().split(',');                        
                        formData[el.id] = keywordArray;
                    } else {
					    formData[el.id] = $(el).val();
                    }
				}
			});
			//books.push(formData);
			//this.collection.add(new Book(formData));
            this.collection.create(formData);
		}, 
        removeBook: function() {
            console.log("remove Book");
        }
	});


	var book = new Book({
		title: 'Backbone Programming',
		author: 'That Guy',
		keywords: 'brogramming'
	});

	var library = new Library([{title:"JS the good parts", author:"John Doe", releaseDate:"2012", keywords:"JavaScript Programming"},
		{title:"CS the better parts", author:"John Doe", releaseDate:"2012", keywords:"CoffeeScript Programming"},
		{title:"Scala for the impatient", author:"John Doe", releaseDate:"2012", keywords:"Scala Programming"},
		{title:"American Psyco", author:"Bret Easton Ellis", releaseDate:"2012", keywords:"Novel Splatter"},
		{title:"Eloquent JavaScript", author:"John Doe", releaseDate:"2012", keywords:"JavaScript Programming"}]);

	var bookView = new BookView({model:book});
	var libraryView = new LibraryView ({collection: new Library()});


